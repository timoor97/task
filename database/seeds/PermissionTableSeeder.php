<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\AdminModels\Permission;
use Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        ////////////////////////////////////////////////////////////////////////////
        /**
         * Users
         */
        $firstOrcreate = Permission::where('slug', 'user-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "user-index",
                "slug" => "user-index",
                "description" => json_encode([
                    "uz"=>"Foydalanuvchi bo'limi",
                    "en"=>"User page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'user-index')->first();
            Permission::insert([
                "name" => "user-create",
                "slug" => "user-create",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchi qoshish imkonini",
                            "en"=>"Can access create user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-edit",
                "slug" => "user-edit",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchini ozgartirish imkoniyati",
                            "en"=>"Can access edit user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-show",
                "slug" => "user-show",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchini korish imkoniyati",
                            "en"=>"Can access show user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-delete",
                "slug" => "user-delete",
                "description" => json_encode([
                            "uz"=>"Royhatdan otgan foydalanuvchini ochirish imkoni",
                            "en"=>"Can access delete user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
         ////////////////////////////////////////////////////////////////////////////
        /**
         * Roles
         */
        $firstOrcreate = Permission::where('slug', 'role-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "role-index",
                "slug" => "role-index",
                "description" => json_encode([
                    "uz"=>"Rol bo'limi",
                    "en"=>"Role page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'role-index')->first();
            Permission::insert([
                "name" => "role-create",
                "slug" => "role-create",
                "description" => json_encode([
                            "uz"=>"Rol qoshish imkonini beradi",
                            "en"=>"Can access create role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-edit",
                "slug" => "role-edit",
                "description" => json_encode([
                            "uz"=>"Rolni ozgartirish imkoniyati",
                            "en"=>"Can access edit Role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-show",
                "slug" => "role-show",
                "description" => json_encode([
                            "uz"=>"Rolni korish imkoniyati",
                            "en"=>"Can access show role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-delete",
                "slug" => "role-delete",
                "description" => json_encode([
                            "uz"=>"Royhatdan otgan rolni ochirish imkoni",
                            "en"=>"Can access delete Role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
       

    }
}
