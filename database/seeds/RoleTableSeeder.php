<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\AdminModels\Permission;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array(
                "name" => "Administrator",
                "slug" => "admin",
                "description" => json_encode([
                                    "uz"=>"Hamma narsani boshqarish imkoniyati",
                                    "en"=>"Can access all things"
                                ])
            ),
        );

        DB::table('roles')->insert($roles);

        $permissions = Permission::all();
        $role = Role::where('slug', 'admin')->first();
        $role->permissions()->sync($permissions);
    }
}
