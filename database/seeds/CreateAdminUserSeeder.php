<?php

use Illuminate\Database\Seeder;
use App\Models\AdminModels\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $role = DB::table('roles')->where('slug', 'admin')->first();
        User::create([
            'name' => "Admin",
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ])->assignRole($role->id);
    }

}
