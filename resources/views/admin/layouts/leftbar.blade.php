<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('admin.index') }}"><img src="{{asset('images/logo.png')}}"
                    alt="Logo"></a>
            <a class="navbar-brand hidden" href="{{ route('admin.index') }}"><img src="{{asset('images/logo2.png')}}"
                    alt="Logo"></a>
        </div>



        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('admin.index') }}"> <i
                            class="menu-icon fa fa-dashboard"></i>{{__('main_trans.Dashboard')}} </a>
                </li>
                <h3 class="menu-title">{{__('main_trans.Settings')}}</h3>
                <li
                    class="menu-item-has-children dropdown {{ Str::contains(Route::currentRouteName(), 'roles') ? 'show' : Str::contains(Route::currentRouteName(), 'users') ? 'show' : ''}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="{{ Str::contains(Route::currentRouteName(), 'roles') ? 'true' : Str::contains(Route::currentRouteName(), 'users') ? 'true' : 'false'}}">
                        <i class="menu-icon fa fa-table"></i>HR</a>
                    <ul class="sub-menu children dropdown-menu {{ Str::contains(Route::currentRouteName(), 'roles') ? 'show' : Str::contains(Route::currentRouteName(), 'users') ? 'show' : ''}}">

                        @can('role-index')
                        <li><i class="fa fa-table"></i>
                            <a href="{{ route('admin.roles.index') }}">{{__('main_trans.Role')}}</a>
                        </li>
                        @endcan

                        @can('user-index')

                        <li><i class="fa fa-table"></i>
                            <a href="{{ route('admin.users.index') }}">{{__('main_trans.User')}}</a>
                        </li>
                        @endcan

                    </ul>
                </li>

                {{-- <li>
                    @can('category-index')
                    <a href="{{ route('admin.category.index') }}"> <i
                            class="menu-icon fa fa-table"></i>{{__('main_trans.Category')}} </a>
                    @endcan
                </li> --}}

            </ul>
        </div>
    </nav>
</aside>
