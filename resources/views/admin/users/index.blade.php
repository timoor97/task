@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.User')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.User')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">{{__('main_trans.User')}}</strong>
                    </div>

                    <div class="card-body">
                        @can('user-create')
                        <a class="btn btn-primary btn-sm" href="{{ route('admin.users.create') }}" style="float: right">
                            {{__('main_trans.Create')}}</a>
                        @endcan

                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>{{__('main_trans.Name')}}</th>
                                    <th>{{__('main_trans.Email')}}</th>
                                    <th>{{__('main_trans.Role')}}</th>
                                    <th>{{__('main_trans.Actions')}}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($data as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success">{{ $v }}</label>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td style="float:right">
                                        <a type="button" class="btn btn-secondary btn-sm"
                                            href="{{ route('admin.users.show',$user->id) }}">{{__('main_trans.Show')}}</a>
                                        @can('user-edit')
                                        <a type="button" class="btn btn-warning btn-sm"
                                            href="{{ route('admin.users.edit',$user->id) }}">{{__('main_trans.Edit')}}</a>
                                        @endcan

                                        @can('user-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.users.destroy',
                                        $user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit(__('main_trans.Delete'), ['class' => 'btn btn-danger btn-sm'])
                                        !!}
                                        {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>


                        {{-- {!! $data->render() !!} --}}
                    </div>

                </div>
            </div>

        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
