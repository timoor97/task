@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.User')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.User')}}/{{__('main_trans.Show')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <div class="pull-right" style="float:right">
            <a class="btn btn-primary" href="{{ route('admin.users.index') }}"> {{__('main_trans.Back')}}</a>
        </div>
            <div class="mx-auto d-block">
                <img class="rounded-circle mx-auto d-block" src="{{asset('images/admin.jpg')}}" alt="Card image cap">
                <h5 class="text-sm-center mt-2 mb-1">{{__('main_trans.Name')}}: {{ $user->name }}</h5>
                <h5 class="text-sm-center mt-2 mb-1">{{__('main_trans.Email')}}: {{ $user->email }}</h5>
                <h5 class="text-sm-center mt-2 mb-1">{{__('main_trans.Role')}}:
                    @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                    <label class="badge badge-success">{{ $v }}</label>
                    @endforeach
                    @endif
                </h5>
                <!-- <div class="location text-sm-center"><i class="fa fa-map-marker"></i> California, United States</div> -->
            </div>
            <hr>
        </div>
    </div>
</div>

@endsection
