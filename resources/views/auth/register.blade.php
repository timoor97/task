@include('site.layouts.links')

<div class="main-content space-60">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Register</li>
        </ul>
        <div class="checkout-header">
            <ul>
                <li class="active">Register</li>

            </ul>
        </div>
        <!-- End checkout header -->
        <div class="account">
            <div class="col-md-12">
                <div class="title-product">
                    <h3>Create a new account</h3>
                </div>
                <!-- End title product -->
                <div class="contact-form check-out acc-login">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label class=" control-label" for="inputemail">User Name</label>
                            <input class="form-control" placeholder="User Name" id="name" name="name"
                                value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class=" control-label" for="inputemail">Email</label>
                            <input type="email" class="form-control" placeholder="Email" id="email" name="email"
                                value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class=" control-label" for="inputpass">Password</label>
                            <input type="password" class="form-control" placeholder="Password" id="password"
                                name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class=" control-label" for="inputpass">Confirm Password</label>
                            <input type="password" class="form-control" placeholder="Password" id="password"
                                name="password_confirmation" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group">
                        <button type="submit" class="btn link-button link-button-v2">Register</button>
                        </div>
                    </form>
                </div>
                <!-- End contact form -->
            </div>

        </div>

 @include('site.layouts.scripts')
