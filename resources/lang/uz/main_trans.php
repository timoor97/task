<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Dashboard' => 'Boshqaruv paneli',
    'Category' => 'Toifa',
    'UnderCategory' => 'Toifalarni toifasi',
    'Tag' => 'Yorliq',
    'Role' => 'Rol',
    'User' => 'Foydalanuvchi',
    'Data Table' => 'Malumotlar jadvali',
    'Show' => 'Korsatish',
    'Search' => 'Qidirmoq',
    'Name' => 'Ism',
    'Guard name' => 'Qoriqchi nomi',
    'Description' => 'Tavsif',
    'Actions' => 'Amallar',
    'Edit' => 'Tahrirlash',
    'Delete' => 'Ochirish',
    'Create' => 'Yaratish',
    'Settings' => 'Sozlamalar',
    'Email' => 'Elektron pochta',
    'Permissions' => 'Ruxsatlar',
    'Submit' => 'Topshirish',
    'Back' => 'Orqaga',
    'Password' => 'Parol',
    'Confirm Password' => 'Parolni tasdiqlang',
    'Next' => 'Keyingisi',
    'Previous' => 'Oldingi',
    'First' => 'Birinchi',
    'Last' => 'Oxirgi',
    'Last' => 'Oxirgi',
    'Showing' => 'Korsatilmoqda',
    'To' => 'Dan',
    'Of' => 'Gacha',
    'Entries' => 'Jami',
];
