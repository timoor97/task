<?php

namespace App\Repositories\AdminRepository;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\AdminInterfaces\UserInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\AdminModels\Role;
use App\Models\AdminModels\Permission;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\User;
use Hash;
use DB;
/**
 * Class RoleRepository.
 */
class UserRepository implements UserInterface
{
    /**
     * Model  variable
     *
     * @var [type]
     */
    protected $model;

    /**
     * Show Pages
     *
     * @var [type]
     */
    protected $per_page;

    /**
     * For initilizing
     *
     * @param Role $model
     */
    public function __construct(User $model) {
        $this->model = $model;
        $this->per_page = request('per_page')?: 100000;
    }

    /**
     * @return string
     *  Return the model
     */
    public function indexInterface()
    {
        return  $this->model->orderBy('id','DESC')->paginate($this->per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function createInterface()
    {
        $roles = Role::pluck('name','name')->all();

        return $roles;
    }

     /**
     * show method find by id
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id)
    {
        $user = $this->model->find($id);

        return $user;
    }

    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model
    {
        $data['password'] = Hash::make($data['password']);

        $user = $this->model->create(
            [
                'name'    => $data['name'],
                'email'    => $data['email'],
                'password'    => $data['password'],
            ]
        );

        $user->assignRole($data['roles']);

        return $user;
    }

     /**
     * Show the form for editing the specified resource
     *
     * @param [type] string
     * @return void
     */
    public function editInterface(int $id)
    {
        $user = $this->model->find($id);


        if(!$user){
            abort(404);
        }

        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return [$user , $roles , $userRole];
    }



    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id)
    {
        if(!empty($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }else{
            $data = array_except($data,array('password'));
        }
        $user = $this->model->find($id);

        $user->update($data);

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($data['roles']);

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     * @return void
     */
    public function destroyInterface(int $id)
    {
        $user = $this->model->find($id);

        if(!$user){
            abort(404);
        }
        $user->delete();

        return $user;

    }
}
