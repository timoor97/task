<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ], function(){

        Route::post('/user/logout', function () {
            Auth::logout();
        return redirect()->route('login');
        })->name('user-logout');

        Auth::routes();

        Route::get('/', 'AdminController@index')->name('admin.index');


        Route::group(['prefix' => 'admin'], function () {

            Route::get('/', 'AdminController@index')->name('admin.index');

            Route::get('/adminlogin', 'AdminControllers\AdminLoginController@index')->name('adminlogin');
            Route::post('/adminPostLogin', 'AdminControllers\AdminLoginController@login')->name('adminPostLogin');

            Route::prefix('roles')->group(function () {
                Route::get('/', 'AdminControllers\RoleController@index')->name('admin.roles.index');
                Route::get('/create', 'AdminControllers\RoleController@create')->name('admin.roles.create');
                Route::post('/store', 'AdminControllers\RoleController@store')->name('admin.roles.store');
                Route::get('/show/{id}', 'AdminControllers\RoleController@show')->name('admin.roles.show');
                Route::get('/edit/{id}', 'AdminControllers\RoleController@edit')->name('admin.roles.edit');
                Route::put('/update/{id}', 'AdminControllers\RoleController@update')->name('admin.roles.update');
                Route::delete('/destroy/{id}', 'AdminControllers\RoleController@destroy')->name('admin.roles.destroy');
            });

            Route::prefix('users')->group(function () {
                Route::get('/', 'AdminControllers\UserController@index')->name('admin.users.index');
                Route::get('/create', 'AdminControllers\UserController@create')->name('admin.users.create');
                Route::post('/store', 'AdminControllers\UserController@store')->name('admin.users.store');
                Route::get('/show/{id}', 'AdminControllers\UserController@show')->name('admin.users.show');
                Route::get('/edit/{id}', 'AdminControllers\UserController@edit')->name('admin.users.edit');
                Route::put('/update/{id}', 'AdminControllers\UserController@update')->name('admin.users.update');
                Route::delete('/destroy/{id}', 'AdminControllers\UserController@destroy')->name('admin.users.destroy');
            });

        });

    });

